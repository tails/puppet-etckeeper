module Etckeeper =
  autoload xfm

  let xfm = transform Shellvars.lns (incl "/etc/etckeeper/etckeeper.conf")
