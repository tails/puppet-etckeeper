class etckeeper::augeas::lens {
  file { '/usr/local/share/augeas': ensure => directory }
  file { '/usr/local/share/augeas/lenses': ensure => directory }
  file { '/usr/local/share/augeas/lenses/etckeeper.aug':
    ensure => present,
    source => 'puppet:///modules/etckeeper/augeas/lenses/etckeeper.aug',
    owner  => root,
    group  => 0,
    mode   => '0644',
  }
}
