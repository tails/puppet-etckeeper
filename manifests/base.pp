class etckeeper::base {
  package { 'etckeeper':
    ensure  => installed,
    require => Class['etckeeper::gitignore']
  }
}
