class etckeeper::autocommits (
  $daily_autocommits     = true,
  $commit_before_install = true,
) {

  case $daily_autocommits {
    true: {
      etckeeper::config { 'etckeeper-enable-daily-autocommits':
        key   => 'AVOID_DAILY_AUTOCOMMITS',
        value => 0,
      }
    }
    default: {
      etckeeper::config { 'etckeeper-disable-daily-autocommits':
        key   => 'AVOID_DAILY_AUTOCOMMITS',
        value => 1,
      }
    }
  }

  case $commit_before_install {
    true: {
      etckeeper::config { 'etckeeper-enable-commit-before-install':
        key   => 'AVOID_COMMIT_BEFORE_INSTALL',
        value => 0,
      }
    }
    default: {
      etckeeper::config { 'etckeeper-disable-commit-before-install':
        key   => 'AVOID_COMMIT_BEFORE_INSTALL',
        value => 1,
      }
    }
  }

}
