class etckeeper::gitignore (
  $ignore_bonus   = true,
  $ignore_secrets = true,
  $custom_ignores = [],
  $gitignore_file = '/etc/.gitignore',
) {

  # init internal data
  $bonus_ignores  = [
    '*.cdb', '*.elc', 'aliases.db', 'postfix/*.db',
    'sv/*/log/main/',
  ]
  $secret_ignores = [
    '*.key', 'gshadow*', 'shadow*', 'ssh/*key', 'sasl_passwd',
  ]

  # Initialize /etc/.gitignore if it does not exist to make sure it
  # contains the usual etckeeper markers. Else, on fresh install,
  # etckeeper refuses to add its own standard ignore lines because
  # this file already exists and lacks the usual markers.
  # Note: the file source is deliberately left hard to customize:
  # the custom_ignores parameter is the preferred way to add custom
  # lines to the Git ignore list.
  file { $gitignore_file:
    source  => 'puppet:///modules/etckeeper/gitignore',
    replace => false,
    owner   => root,
    group   => root,
    mode    => '0600',
    before  => [
      Etckeeper::Gitignore_line[$bonus_ignores],
      Etckeeper::Gitignore_line[$secret_ignores],
      Etckeeper::Gitignore_line[$custom_ignores],
    ],
  }

  if $ignore_bonus {
    etckeeper::gitignore_line { $bonus_ignores: }
  }

  if $ignore_secrets {
    etckeeper::gitignore_line { $secret_ignores: }
  }

  if $custom_ignores {
    etckeeper::gitignore_line { $custom_ignores: }
  }

}
