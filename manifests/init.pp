class etckeeper (
  $ignore_bonus          = true,
  $ignore_secrets        = true,
  $custom_ignores        = [],
  $daily_autocommits     = true,
  $commit_before_install = true,
) {

    class { '::etckeeper::gitignore':
      ignore_bonus   => $ignore_bonus,
      ignore_secrets => $ignore_secrets,
      custom_ignores => $custom_ignores,
    }

    case $::operatingsystem {
        debian,ubuntu: { include etckeeper::debian }
        default: { include etckeeper::default }
    }

    class { '::etckeeper::autocommits':
      daily_autocommits     => $daily_autocommits,
      commit_before_install => $commit_before_install,
    }

    cron { 'cleanup-git-in-etc':
      user    => 'root',
      minute  => fqdn_rand(60, 'etckeeper-git-gc'),
      hour    => fqdn_rand(24, 'etckeeper-git-gc'),
      command => 'git -C /etc gc --auto --quiet',
      require => Package['etckeeper'],
    }
}
