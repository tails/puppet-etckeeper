define etckeeper::gitignore_line {
  file_line { $name:
    path => $etckeeper::gitignore::gitignore_file,
    line => $name,
  }
}
