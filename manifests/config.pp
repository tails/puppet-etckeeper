define etckeeper::config ( $key, $value ) {

  include etckeeper::augeas::lens

  $etckeeper_config = '/etc/etckeeper/etckeeper.conf'

  augeas { "etckeeper-${name}":
    context   => "/files${etckeeper_config}",
    changes   => "set ${key} ${value}",
    require   => [ Package[etckeeper], Class['etckeeper::augeas::lens'] ],
    load_path => '/usr/local/share/augeas/lenses',
  }

}
